package main

import (
	"bitbucket.org/heckd3v/forframework/core"
	"bitbucket.org/heckd3v/forframework/formother"
	"flag"
	"fmt"
	_ "github.com/dimiro1/banner/autoload"
	"os"
	"os/signal"
	"syscall"
)

var (
	killPill         = make(chan os.Signal)
	terminateProcess = make(chan chan bool)
)

func main() {
	var apiPort int64
	var enableEcho bool
	flag.Int64Var(&apiPort, "apiPort", 8090, "API Port where API Function listens")
	flag.BoolVar(&enableEcho, "enableEcho", true, "Enable/Disable Default Echo Function")

	flag.Parse()
	core.GetCLIFlags().APIPort = apiPort
	core.GetCLIFlags().EnableEcho = enableEcho

	var terminateFORSignal chan bool
	//Initialize f.o.r mother controller
	go formother.StartForMother(core.GetTerminateFORChannel(), terminateProcess)
	signal.Notify(killPill, os.Interrupt, syscall.SIGINT, syscall.SIGTERM, syscall.SIGSTOP, syscall.SIGTSTP)
	for {
		select {
		case <-killPill:
			fmt.Println(fmt.Sprintf("\n\n****** - Shutting down due to SIGTERM - ******"))
			formother.ProcessKillPill(terminateFORSignal)
			fmt.Println("\n\n****** - SIGTERM Processed - ******")
			return
		case <-terminateFORSignal:
			core.PurgeFOR()
		case <-terminateProcess:
			fmt.Println("\n\n****** - Terminate process command received - ******")
			formother.ProcessKillPill(terminateFORSignal)
			return
		}
	}
}
