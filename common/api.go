package common

import (
	coreModels "bitbucket.org/heckd3v/forframework/core"
	"fmt"
	"html/template"
	"net/http"
	"path/filepath"
	"syscall"
)

var (
	Templates        = make(map[string]*template.Template)
	loginFormDetails = make(map[string]interface{})
	motherFORChannel chan coreModels.FORRequest
)

func init() {
	filePrefix, err := filepath.Abs("view/templates/")
	if err != nil {
		fmt.Println(fmt.Sprintf("Error in reading template files : %v", err.Error()))
		syscall.Exit(1)
	}
	//Templates["Login"] = template.Must(template.ParseFiles(filePrefix + "/login.html"))
	Templates["Login"] = template.Must(template.ParseFiles(filePrefix+"/login.html", filePrefix+"/layouts/theme.html"))
	//views, err := filepath.Glob(filePrefix + "/*.html")
	//if err != nil {
	//	log.Fatal("Error in reading template file(s) : %v", err.Error())
	//}
	//for _, view := range views {
	//	fmt.Println(fmt.Sprintf("Adding template key %v", filepath.Base(view)))
	//	Templates[filepath.Base(view)] = template.Must(template.ParseFiles(view, "bitbucket.org/heckd3v/clibot/view/templates/layouts/theme.html"))
	//}

	// Populate form details
	loginFormDetails["Title"] = "FOR Catalogue"
	loginFormDetails["Message"] = "Browse your catalogue ..."
}

func listHandler(w http.ResponseWriter, r *http.Request) {
	req := coreModels.FORRequest{}
	req.Request = "?$List"
	resp := make(chan interface{})
	req.Response = resp
	motherFORChannel <- req
	fmt.Fprintf(w, "%v", <-resp)
}

func pingHandler(w http.ResponseWriter, r *http.Request) {
	fmt.Fprintf(w, "API Function up!!!")
}

func loginHandler(w http.ResponseWriter, r *http.Request) {
	if r.Method == http.MethodGet {
		t := Templates["Login"]
		if t == nil {
			fmt.Fprintf(w, "Error in finding a valid template file  for key %s", "Login")
		} else {
			t.ExecuteTemplate(w, "outerTheme", loginFormDetails)
		}
	} else {
		err := r.ParseForm()
		if err != nil {
			fmt.Fprintf(w, "Error in processing form input : %v", err.Error())
		} else {
			botId := r.Form["email"]
			req := coreModels.FORRequest{}
			req.Request = "?$Init@#" + botId[0]
			resp := make(chan interface{})
			req.Response = resp
			motherFORChannel <- req
			fmt.Println(fmt.Sprintf("Response is %v", <-resp))
			req = coreModels.FORRequest{}
			req.Request = LIST_FORS
			resp = make(chan interface{})
			req.Response = resp
			motherFORChannel <- req
			fmt.Fprintf(w, "%v", <-resp)
		}
	}
}

func ApiFOR(port int64, terminateChannel <-chan chan bool, motherFOR chan coreModels.FORRequest) {
	motherFORChannel = motherFOR
	http.HandleFunc("/listFORs", listHandler)
	http.HandleFunc("/ping", pingHandler)
	http.HandleFunc("/login", loginHandler)
	http.HandleFunc("/", loginHandler)
	http.Handle("/view/bootstrap/", http.StripPrefix("/view/bootstrap/", http.FileServer(http.Dir("view/bootstrap"))))
	go http.ListenAndServe(fmt.Sprintf(":%v", port), nil)
	fmt.Println(fmt.Sprintf("API Function started, Listening on port '%v', protocol 'http'", port))
	for {
		select {
		case terminateStatus := <-terminateChannel:
			fmt.Println("API Function shutting down due to SIGTERM")
			terminateStatus <- true
			return
		}
	}
}
