package common

import (
	"bitbucket.org/heckd3v/forframework/core"
	"encoding/json"
	"fmt"
	"strings"
)

func initCommandLineFOR(id string, pings <-chan core.FORRequest, terminateFORSignal <-chan chan bool) {
	for {
		select {
		case ping := <-pings:
			response := ping.Response
			resp, _ := json.Marshal(map[string]interface{}{"forID": id, "response": ping.Request})
			response <- string(resp)
			if ping.Request == "Exit" && strings.TrimSpace(id) != "EchoFOR" {
				fmt.Println(fmt.Sprintf("%v shutting down", id))
				core.IncinerateFOR(id)
				terminateStatus := <-terminateFORSignal
				terminateStatus <- true
				return
			}
		case terminateStatus := <-terminateFORSignal:
			fmt.Println(fmt.Sprintf("%v shutting down due to explicit terminate signal", id))
			terminateStatus <- true
			return
		}
	}
}

func processCommands(cmdReq core.FORRequest) {
	if strings.HasPrefix(cmdReq.Request, INIT_FOR) {
		splitId := strings.SplitAfter(cmdReq.Request, "@#")
		if len(splitId) <= 1 || strings.TrimSpace(splitId[1]) == "" {
			fmt.Println(core.CANNOT_CREATE_FOR_INVALID_SYNTAX)
			return
		}
		forId := splitId[1]
		fmt.Println("Starting a new function with forId : " + forId)
		forChannel := make(chan core.FORRequest, 5)
		go initCommandLineFOR(forId, forChannel, core.GetTerminateFORChannel())
		core.CreateFOR()
		core.AddFORChannel(forId, forChannel)
		resp, _ := json.Marshal(map[string]interface{}{"response": fmt.Sprintf("Created a function with forId %v", forId)})
		cmdReq.Response <- string(resp)
		return
	}
	if cmdReq.Request == LIST_FORS {
		resp, _ := json.Marshal(core.GetFORList())
		cmdReq.Response <- string(resp)
		return
	}
}

func EchoFOR(terminateChannel <-chan chan bool) {
	//Creating a persistent echo function
	fmt.Println("Echo Function started")
	botChannel := make(chan core.FORRequest, 5)
	go initCommandLineFOR("EchoFOR", botChannel, terminateChannel)
	//botChannels["EchoFOR"] = botChannel
	core.AddFORChannel("EchoFOR", botChannel)
}

func CommandFOR(motherFORCommand chan core.FORRequest, terminateChannel <-chan chan bool) {
	fmt.Println("Command Function started")
	for {
		select {
		case cmd := <-motherFORCommand:
			processCommands(cmd)
		case terminateStatus := <-terminateChannel:
			fmt.Println(fmt.Sprintf("CommandFOR shutting down due to SIGTERM"))
			terminateStatus <- true
			return
		}
	}
}
