package formother

import (
	"bitbucket.org/heckd3v/forframework/common"
	"bitbucket.org/heckd3v/forframework/core"
	"bufio"
	"fmt"
	"io"
	"os"
	"strings"
)

var (
	terminateMotherFOR = make(chan bool)
	motherFORChannel   chan core.FORRequest
)

func read(r io.Reader) chan core.FORRequest {
	lines := make(chan core.FORRequest)
	go func() {
		defer close(lines)
		scanLines := bufio.NewScanner(r)
		for scanLines.Scan() {
			scannedLine := scanLines.Text()
			req := core.FORRequest{}
			req.Request = scannedLine
			lines <- req
		}
	}()
	return lines
}

func createBasicInterface(motherFORCommandChannel chan core.FORRequest) {
	createCommandFOR(motherFORCommandChannel)
	createApiFOR(core.GetCLIFlags().APIPort, motherFORChannel)
	createEchoFOR()
}

// StartForMother - Starts the F.O.R (Function Over Routine) mother controller
func StartForMother(terminateFORChannel <-chan chan bool, terminateProcess chan chan bool) {
	motherFORChannel = read(os.Stdin)
	motherFORCommandChannel := make(chan core.FORRequest)
	createBasicInterface(motherFORCommandChannel)
	for {
		select {
		case request := <-motherFORChannel:
			if request.Response == nil {
				request.Response = make(chan interface{})
				go cliResponeFOR(request.Response, terminateFORChannel)
			}
			response := request.Response
			if strings.HasPrefix(request.Request, "?$") {
				if request.Request == "?$KillPill" {
					SendKillPill(terminateProcess)
				} else {
					motherFORCommandChannel <- request
				}
			} else {
				if !core.IsAnyFORAvailable() && !core.GetCLIFlags().EnableEcho {
					response <- core.NO_FOR_CREATED
				} else {
					cmdRoute := strings.SplitAfter(request.Request, "@#")
					if len(cmdRoute) == 0 {
						response <- core.FOR_ID_NOT_SPECIFIED
					} else {
						forID := strings.TrimSuffix(cmdRoute[0], "@#")
						if strings.TrimSpace(forID) == "" && core.GetCLIFlags().EnableEcho {
							forID = "EchoFOR"
							request.Request = strings.Join(cmdRoute[1:], "")
							core.SendToFOR(forID, request)
						} else {
							if core.FetchFOR(forID) == nil {
								response <- fmt.Sprintf(core.INVALID_FOR_ID, forID)
							} else {
								request.Request = strings.Join(cmdRoute[1:], "")
								core.SendToFOR(forID, request)
							}
						}
					}
				}
			}
		case <-terminateMotherFOR:
			return
		}
	}
}

func SendKillPill(terminateProcess chan chan bool) {
	terminateProcess <- make(chan bool)
}

func ProcessKillPill(terminateFORSignal chan bool) {
	fmt.Println("\n\n****** - Processing KillPill - ******")
	core.AcquireLock()
	defer core.ReleaseLock()
	terminateFORSignal = make(chan bool, core.GetActiveFORs())
	for i := 0; i < core.GetActiveFORs(); i++ {
		core.GetTerminateFORChannel() <- terminateFORSignal
	}
	terminateMotherFOR <- true
	fmt.Println("\n\n****** - KillPill success - ******")
}

func cliResponeFOR(response chan interface{}, terminateChannel <-chan chan bool) {
	core.CreateFOR()
	for {
		select {
		case pong := <-response:
			fmt.Println(fmt.Sprintf("%v", pong))
			core.PurgeFOR()
			return
		case terminateStatus := <-terminateChannel:
			fmt.Println(fmt.Sprintf("ResponseFOR shutting down due to SIGTERM"))
			terminateStatus <- true
			return
		}
	}
}

func createApiFOR(port int64, motherFORChannel chan core.FORRequest) {
	core.CreateFOR()
	go common.ApiFOR(port, core.GetTerminateFORChannel(), motherFORChannel)
}

func createCommandFOR(redirectToMotherFOR chan core.FORRequest) {
	core.CreateFOR()
	go common.CommandFOR(redirectToMotherFOR, core.GetTerminateFORChannel())
}

func createEchoFOR() {
	core.CreateFOR()
	go common.EchoFOR(core.GetTerminateFORChannel())
}
