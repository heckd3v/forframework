package core

const (
	NO_FOR_CREATED                   = "No Function yet created, please use command ?$Init@#<userid> to create a handler function"
	FOR_ID_NOT_SPECIFIED             = "Cannot route to a valid FOR, id not specified. Syntax -> <FORId>@#<Message>"
	INVALID_FOR_ID                   = "Invalid FOR id %v"
	CANNOT_CREATE_FOR_INVALID_SYNTAX = "Not able to create Function as forID cannot be empty. Use  ?$Init@#<userid> to create a handler function"
)
