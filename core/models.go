package core

// FOR - Function Over Routine core structure
type FOR struct {
	FORID string `json:"Name"`
}

// FOR - Function Over Routine request structure
type FORRequest struct {
	Request  string
	Response chan interface{}
}

// FOR - Function Over Routine response structure
type FORResponse struct {
	BotID    string
	Response interface{}
}

type CLIFlags struct {
	APIPort    int64 `json:"APIPort"`
	EnableEcho bool  `json"EnableEcho"`
}
