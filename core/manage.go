package core

import "sync"

var (
	activeFORs          int
	mutex               sync.Mutex
	forChannels         = make(map[string]chan FORRequest)
	terminateFORChannel = make(chan chan bool)
	cliFlags            = CLIFlags{}
)

func CreateFOR() {
	mutex.Lock()
	activeFORs++
	mutex.Unlock()
}

func PurgeFOR() {
	mutex.Lock()
	activeFORs--
	mutex.Unlock()
}

func IncinerateFOR(forID string) {
	mutex.Lock()
	delete(forChannels, forID)
	mutex.Unlock()
}

func GetFORList() []FOR {
	fors := []FOR{}
	for forID, _ := range forChannels {
		fors = append(fors, FOR{forID})
	}
	return fors
}

func GetTerminateFORChannel() chan chan bool {
	return terminateFORChannel
}

func GetActiveFORs() int {
	return activeFORs
}

func AddFORChannel(forID string, forChannel chan FORRequest) {
	forChannels[forID] = forChannel
}

func IsAnyFORAvailable() bool {
	return len(forChannels) > 0
}

func SendToFOR(forID string, forRequest FORRequest) {
	forChannels[forID] <- forRequest
}

func FetchFOR(forID string) chan FORRequest {
	return forChannels[forID]
}

func AcquireLock() {
	mutex.Lock()
}

func ReleaseLock() {
	mutex.Unlock()
}

func GetCLIFlags() *CLIFlags {
	return &cliFlags
}
